# Site Policy

## License

To the extent possible under law, Recap Time squad members and contributors has waived all copyright and related or neighboring rights to Site Policy at recaptime.app. This work is published from Philippines and around the world thanks to contributors and our legal team, for both the source files in this repository and also the website contents at `policy.recaptime.tk`.

Full legal code can be obtained at the LICENSE file or through <https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt>
